﻿using UnityEngine;
using System.Collections;

public class GrammaAnimation : MonoBehaviour {
	public AstarAI aStarAI;
	private SkeletonAnimation skeletonAnimation;

	// Use this for initialization
	void Start () {
		skeletonAnimation = GetComponent<SkeletonAnimation>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Mathf.Abs(aStarAI.grammaDirection.x) > Mathf.Abs(aStarAI.grammaDirection.z)) {
			if (aStarAI.grammaDirection.x < 0) {
				skeletonAnimation.state.SetAnimation(0, "left", true);
			}
			else {
				skeletonAnimation.state.SetAnimation(0, "right", true);
			}
		}
		else {
			if (aStarAI.grammaDirection.z > 0) {
				skeletonAnimation.state.SetAnimation(0, "back", true);
			}
			else {
				skeletonAnimation.state.SetAnimation(0, "front", true);
			}
		}
	}
}
