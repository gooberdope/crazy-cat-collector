﻿using UnityEngine;
using System.Collections;

public class GetScreenCoordinate : MonoBehaviour {
	
	public Vector3 targetCoordinate;

	// Use this for initialization
	void Start () {

	}

	void Update () {
		// Construct a ray from the current mouse coordinates
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		int layerMask = (1 << 8) | (1 << 11);

		if (Input.GetButton ("Fire1")) {
			if (Physics.Raycast (ray, out hit, Mathf.Infinity, layerMask)) {
				if (hit.collider.CompareTag("Ground Tile")) {
					targetCoordinate.x = hit.collider.transform.position.x;
					targetCoordinate.z = hit.collider.transform.position.z;
				}
			}
		}

		if (Input.GetButtonDown ("Fire1")) {
			if (Physics.Raycast (ray, out hit, Mathf.Infinity, layerMask)) {
				if (hit.collider.CompareTag("Rat")) {
					hit.collider.gameObject.SendMessage("Tapped");
				}
			}
		}
	}
	
	public Vector3 GetTargetCoordinate () {
		return targetCoordinate;
	}
}
