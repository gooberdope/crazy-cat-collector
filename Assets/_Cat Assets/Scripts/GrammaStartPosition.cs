﻿using UnityEngine;
using System.Collections;

public class GrammaStartPosition : MonoBehaviour {
	public Vector3 initialPosition;
	public float health = 100;
	public float healthTotal = 100;
	public GUIStyle progress_empty;
	public GUIStyle progress_full;
	public float barDisplay;
	public Texture2D emptyTex;
	public Texture2D fullTex;
	//private float percentage;
	private float sizeX;
	private float sizeY;
	private float BarHeight;
	private float BarWidth;
	private float Tony=9.63f;
	private float Jared=10.85f;


	void Update () {
		barDisplay = health/healthTotal;
		if (health <= 0 ){
			Application.LoadLevel(0);
		}
		sizeX = (Screen.width/12)*Tony;
		sizeY = (Screen.height/12)*Jared;
		BarHeight = (Screen.height/14);
		BarWidth = (BarHeight*4);

	}

		
		//current progress

		
	//Vector2 pos = new Vector2 (10, 50);
	//Vector2 size = new Vector2 (250,50);
		

		
	void OnGUI()
	{
		//draw the background:
		//GUI.BeginGroup(new Rect(pos.x, pos.y, size.x, size.y), emptyTex, progress_empty);

		GUI.DrawTexture(new Rect(sizeX, sizeY, BarWidth, BarHeight), emptyTex, ScaleMode.ScaleAndCrop, true, 0);
			
		//draw the filled-in part:
		//GUI.BeginGroup(new Rect(0, 0, 250, 50));
			
		GUI.DrawTexture(new Rect(sizeX, sizeY, (BarWidth*(barDisplay)), BarHeight), fullTex, ScaleMode.ScaleAndCrop, true, 0);
			
		//GUI.EndGroup();
		//GUI.EndGroup();
	}
		

		
	
void Start () {
		transform.position = initialPosition;
	}
	
	// Update is called once per frame

}
