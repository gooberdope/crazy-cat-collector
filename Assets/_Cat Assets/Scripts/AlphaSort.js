﻿#pragma strict

// Attach to main camera, renders the layers in scene in orthographic mode, when using perspective.

function Start () {
	gameObject.camera.transparencySortMode = TransparencySortMode.Orthographic;
}
